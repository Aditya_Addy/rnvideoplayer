/*Example of React Native Video*/
import React, { Component } from 'react';
// you can also use video1.js.. that is controlled by different package 
//Import React
import { Platform, StyleSheet, Text, View, TouchableWithoutFeedback } from 'react-native';
import Video from 'react-native-video';
class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      currentTime: 0,
      duration: 0,
      lastPress:0
    };
    this.handlePress = this.handlePress.bind(this);
    this.onProgress = this.onProgress.bind(this);
    this.onLoad = this.onLoad.bind(this);
  }

  onLoad(data){
    this.setState({
      duration:data.duration
    })
    //console.log(data);
  }
  onProgress = data => {
      this.setState({ currentTime: data.currentTime });
    
  };
  handleDoublePressLeft(){
    console.log("left");
    console.log(this.state.currentTime);
    this.player.seek(this.state.currentTime-10);
  }
  handleDoublePressRight(){
    console.log("right");
    this.player.seek(this.state.currentTime+10);
  }
  handlePress(evt){
    const now = new Date().getTime();
    if((now-this.state.lastPress)<300){
      this.setState({
        lastPress:now
      })
      //print("hello")
      const xc = evt.nativeEvent.locationX;
      //console.log(`x coord = ${evt.nativeEvent.locationX}`);
      //console.log(`Y coord = ${evt.nativeEvent.locationY}`);
      if(xc<200){
        this.handleDoublePressLeft();
      }
      else{
        this.handleDoublePressRight();
      }
    }else{
      this.setState({
        lastPress:now
      })
    }

  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableWithoutFeedback onPress={this.handlePress}>
        <Video source={{uri: 'https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8'}}   // Can be a URL or a local file.
       ref={(ref) => {
         this.player = ref;
         
       }}                                      // Store reference
       onBuffer={this.onBuffer}                // Callback when remote video is buffering
       onError={this.videoError} 
       onLoad={this.onLoad}              // Callback when video cannot be loaded
       onProgress={this.onProgress}
       style={styles.backgroundVideo}
       controls={true}
       resizeMode={"contain"}
        />
</TouchableWithoutFeedback>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 0.7,
  },
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});

export default App;


