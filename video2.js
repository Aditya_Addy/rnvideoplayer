/*Example of React Native Video*/
import React, { Component } from 'react';
// you can also use video1.js.. that is controlled by different package 
//Import React
import { Platform, StyleSheet, Text, View, TouchableWithoutFeedback } from 'react-native';
import VideoPlayer from 'react-native-video-player'
class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      currentTime: 0,
      duration: 0,
      isFullScreen: false,
      isLoading: true,
      paused: false,
      playerState: "PLAYER_STATES.PLAYING",
      screenType: 'content',
      lastPress:0
    };
    this.handlePress = this.handlePress.bind(this);
  }

  handleDoublePress(){

  }
  handlePress(evt){
    const now = new Date().getTime();
    //this.setState({
    //  currentTime:this.state.currentTime+10
    //})
    //console.log(now-this.state.lastPress);
    console.log(this.state.currentTime);
    if((now-this.state.lastPress)<300){
      this.setState({
        lastPress:now
      })
      //print("hello")
      console.log(`x coord = ${evt.nativeEvent.locationX}`);
      console.log(`Y coord = ${evt.nativeEvent.locationY}`);
      //this.handleDoublePress();
      
    }else{
      this.setState({
        lastPress:now
      })
    }

  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableWithoutFeedback onPress={this.handlePress}>
        <View>
        <VideoPlayer
    video={require('./FIFA.mp4')}
    videoWidth={1600}
    videoHeight={2000}
    thumbnail={{ uri: 'https://i.picsum.photos/id/866/1600/900.jpg' }}
>

  </VideoPlayer>
</View>
</TouchableWithoutFeedback>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
export default App;