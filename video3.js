/*Example of React Native Video*/
import React, { Component } from 'react';
// you can also use video1.js.. that is controlled by different package 
//Import React
import { Platform, StyleSheet, Text, View, TouchableWithoutFeedback } from 'react-native';
import VideoPlayer from 'react-native-video-player'
import Video from 'react-native-video';
class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      currentTime: 0,
      duration: 0,
      isFullScreen: false,
      isLoading: true,
      paused: false,
      playerState: "PLAYER_STATES.PLAYING",
      screenType: 'content',
      lastPress:0
    };
    this.handlePress = this.handlePress.bind(this);
    this.onProgress = this.onProgress.bind(this);
  }

  onProgress = data => {
      this.setState({ currentTime: data.currentTime });
    
  };
  handleDoublePressLeft(){
    console.log("left");
    console.log(this.state.currentTime);
    this.player.seek(this.state.currentTime-10);
  }
  handleDoublePressRight(){
    console.log("right");
    this.player.seek(this.state.currentTime+10);
  }
  handlePress(evt){
    const now = new Date().getTime();
    if((now-this.state.lastPress)<300){
      this.setState({
        lastPress:now
      })
      //print("hello")
      const xc = evt.nativeEvent.locationX;
      //console.log(`x coord = ${evt.nativeEvent.locationX}`);
      //console.log(`Y coord = ${evt.nativeEvent.locationY}`);
      if(xc<200){
        this.handleDoublePressLeft();
      }
      else{
        this.handleDoublePressRight();
      }
      //this.handleDoublePress();
      
    }else{
      this.setState({
        lastPress:now
      })
    }

  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableWithoutFeedback onPress={this.handlePress}>
        <Video source={require('./dvfinal1.mp4')}   // Can be a URL or a local file.
       ref={(ref) => {
         this.player = ref
       }}                                      // Store reference
       onBuffer={this.onBuffer}                // Callback when remote video is buffering
       onError={this.videoError}               // Callback when video cannot be loaded
       onProgress={this.onProgress}
       style={styles.backgroundVideo}
       controls={true}
       resizeMode={"contain"} />
</TouchableWithoutFeedback>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});

export default App;














 
// Within your render function, assuming you have a file called
// "background.mp4" in your project. You can include multiple videos
// on a single screen if you like.
 

 
// Later on in your styles..
